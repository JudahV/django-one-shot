from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm



def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list_list": list,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": list,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id =model_instance.id)

    else:
        form = TodoListForm()

    context = {
        "form": form
    }

    return render(request, "todos/create.html", context)